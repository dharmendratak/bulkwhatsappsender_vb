# Bulkwhatsappsender Vb

Simple Visual Basic project using Excel Macro to send WhatsApp messages in bulk using only WhatsApp Desktop App and MS Excel.

# Prerequisite

To use this script you need 'WhatsApp Desktop App' and MS Excel (Part of MS Office) already installed on your system.

# Installation

No installation needed for script. All required steps are described in details in youtube video

# Support

In case of support needed, or you want to contact me, just mail on dharmendratak.btp@gmail.com or info@cubiclestechnologies.com
